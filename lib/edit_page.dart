import 'package:car_app/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import 'car.dart';
import 'car_data.dart';
import 'car_database_helper.dart';
import 'common.dart';

class EditPage extends StatefulWidget {
  EditPage(this.car, this.carProvider);

  final Car car;
  final CarProvider carProvider;

  @override
  _EditPageState createState() => _EditPageState(car, carProvider);
}

class _EditPageState extends State<EditPage> {
  final Car car;
  final CarProvider carProvider;
  final imagePicker = ImagePicker();

  _EditPageState(this.car, this.carProvider);

  TextEditingController _editControllerCarModel = TextEditingController();
  TextEditingController _editControllerManufacturer = TextEditingController();

  @override
  void initState() {
    super.initState();
    _editControllerCarModel = TextEditingController.fromValue(
      TextEditingValue(text: car.model),
    );
    _editControllerManufacturer = TextEditingController.fromValue(
        TextEditingValue(text: car.manufacturer));
  }

  @override
  void dispose() {
    _editControllerCarModel.dispose();
    _editControllerManufacturer.dispose();
    super.dispose();
  }

  Widget buildDropDownList(
      List<String> values, String selectedValue, Function changeValue) {
    return DropdownButton<String>(
      onChanged: (String newValue) {
        setState(() {
          changeValue(newValue);
        });
      },
      isExpanded: true,
      items: values.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Container(
            padding: EdgeInsets.all(3.0),
            child: Text(
              value,
              style: TextStyle(fontSize: 16.0),
            ),
          ),
        );
      }).toList(),
      value: selectedValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 16.0,
    );
  }

  Widget buildTextField(TextEditingController controller, Function onChanged) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        style: TextStyle(fontSize: 16.0),
      ),
    );
  }

  Widget buildCarData() {
    return Column(
      children: [
        GestureDetector(
          child: Common.getImage(car.imgPath),
          onTap: _pickImage,
        ),
        buildTextField(_editControllerCarModel, (String newValue) {
          if (newValue.isNotEmpty) {
            car.model = newValue;
          }
        }),
        buildTextField(_editControllerManufacturer, (String newValue) {
          if (newValue.isNotEmpty) {
            car.manufacturer = newValue;
          }
        }),
        Container(
            margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
            child: buildDropDownList(CarData.classes, car.carClass,
                (String newCarClass) {
              car.carClass = newCarClass;
            })),
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
          child: Center(
            child: buildDropDownList(
                CarData.years.map((e) => e.toString()).toList(),
                car.year.toString(), (String newYear) {
              car.year = int.parse(newYear);
            }),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
          child: buildDropDownList(CarData.bodyTypes, car.bodyType,
              (String newBodyType) {
            car.bodyType = newBodyType;
          }),
        ),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  void _updateCar() {
    carProvider.update(car);
    Navigator.of(context).pop();
  }

  void _pickImage() {
    Common.showDialogOptions(context, (String itemSelected) {
      if (itemSelected == Strings.cameraAction) {
        Common.pickImageCamera((String path) => setState(() => car.imgPath = path));
      } else {
        Common.pickImageGallery(
                (String path) => setState(() => car.imgPath = path));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(Strings.editTitle), actions: [
        Container(
          margin: EdgeInsets.only(right: 3),
          child: IconButton(
            onPressed: _updateCar,
            icon: Icon(Icons.check),
          ),
        )
      ]),
      body: Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: SingleChildScrollView(
            child: buildCarData(),
          )),
      floatingActionButton: FloatingActionButton(
        tooltip: Strings.pickImageTooltip,
        onPressed: _pickImage,
        child: Icon(
          Icons.add_a_photo,
          color: Color.fromARGB(0xFF, 255, 255, 255),
        ),
      ),
    );
  }
}
