class Strings {
  static String appName = "Car App";

  static final String imgFolder = "images";
  static final String imgPlaceholder = "/car_placeholder.png";

  static final String helpTitle = "Справка";
  static final String editTitle = "Редактирование";
  static final String addTitle = "Добавление";

  static final String addTooltip = "Добавить автомобиль";

  static final String pickImageTooltip = "Выбрать изображение";

  static final String hintModel = "Модель";
  static final String hintManufacturer = "Производитель";

  static final String errorEmptyField = "Поле не может быть пустым";

  static final List<String> imageSelectOptions = ["галерея", "камера"];
  static final String imageSelectTitle = "Выберите вариант";
  static final String imageSelectContent =
      "Вы можете использовать камеру, чтобы сделать фото или выбрать его из галерии";

  static final String deleteTitle = "Удаление записей";
  static final String deleteContent = "Вы действительно хотите удалить";

  static final String cancelAction = "Отмена";
  static final String okAction = "OK";

  static final String cameraAction = "Камера";
  static final String galleryAction = "Галерея";

  static final List<String> aboutCarClasses = [
    "Класс А (до 3,6 м, ширина до 1,6 м)",
    '''Сюда входят "самые маленькие" (super-mini) автомобили, предназначенные для условий тесного города. Тип кузова — обычно 3-дверный, реже 5-дверный хэтчбек. Экономичны, но удобны лишь для двух человек и небольшого багажа. Типичные представители — • Daewoo, Ford Ka, Renault Twingo, Peugeot 106, Kia Picanto.''',
    "Класс В (до 3,9 м, ширина до 1,7 м)",
    '''Популярный в Европе класс малогабаритных машин, считающихся "чисто городскими". Значительная часть отличается кузовом хетчбэк и передним приводом. Представители — • Volkswagen Polo/Classic, Seat Ibiza и Cordoba, Ford Fusion, Fiat Punto, Opel Corsa, Peugeot 206.''',
    "Класс С (до 4,3 м, ширина до 1,7-1,8 м)",
    '''"Гольф-класс" или "низший средний", наиболее популярный в Европе (около 30%). На протяжении десятилетий законодателем мод здесь был Volkswagen Golf. Типичные автомобили — • VW Golf и Bora, Ford Escort и Focus, Audi A3, Mercedes-Benz А-класса, Opel Astra, Peugeot 307, Honda Civic, Hyundai Accent, Toyota Corolla.''',
    "Класс D (до 4,6 м)",
    '''Средний (или «полноценный» средний) класс. Автомобили этого класса, хетчбэки и седаны, многими считаются, и вправе, оптимальным транспортным средством как по вместимости, так и по своим потребительским качествам. Типичные представители — • Audi A4, Opel Vectra, VW Passat, Ford Mondeo, Mercedes-Benz С-класса, BMW 3-й серии, Peugeot 406.''',
    "Класс Е (свыше 4,6 м)",
    '''Высший средний или бизнес-класс. Автомобили класса «Е» отличает высокий уровень комфорта, внушительные размеры и, соответственно, высокая цена. Типичные представители — • Audi А6, BMW 5-й серии, Mercedes-Benz E-класса, Opel Omega, Toyota Camry.''',
    "Класс F (свыше 5 м)",
    '''Комфортабельные эксклюзивные. мощные автомобили, которые обычно называют "люкс" или представительскими. Например, • Rolls-Royce, Jaguar XJ8, Mercedes-Benz S-класса, BMW 7-й серии."''',
  ];
}
