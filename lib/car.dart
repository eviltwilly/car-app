class Car {
  final int id;
  String model;
  String manufacturer;
  String carClass;
  int year;
  String bodyType;
  String imgPath;

  Car(this.model, this.manufacturer, this.carClass, this.year, this.bodyType,
      {this.imgPath = "", this.id = 0});
}
