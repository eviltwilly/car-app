import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'car.dart';
import 'strings.dart';

class CarProvider {
  Database db;

  bool isOpenedDb = false;

  final String dbName = "cars_database";
  final String tableName = "cars";

  final String columnId = "_id";
  final String columnModel = "model";
  final String columnManufacturer = "manufacturer";
  final String columnClass = "class";
  final String columnYear = "year";
  final String columnBodyType = "body_type";
  final String columnImgPath = "img_path";

  Future open(String dbName) async {
    var dbPath = await getDatabasesPath();

    db = await openDatabase(join(dbPath, dbName), version: 1,
        onCreate: (Database db, int version) async {
      await db.transaction((txn) async {
        await db.execute('''
          create table $tableName (
            $columnId integer primary key autoincrement not null,
            $columnModel text not null,
            $columnManufacturer text not null,
            $columnClass varchar(1) not null,
            $columnYear integer not null,
            $columnBodyType varchar(20) not null,
            $columnImgPath varchar(255) not null
          );
        ''');

        await db.rawInsert('''insert into $tableName(
              $columnModel, $columnManufacturer, $columnClass, 
              $columnYear, $columnBodyType, $columnImgPath)
              values("Lada Vesta", "АвтоВаз", "B", 2014,
              "седан", "${Strings.imgFolder + "/lada_vesta.jpg"}")''');

        await db.rawInsert('''insert into $tableName(
              $columnModel, $columnManufacturer, $columnClass, 
              $columnYear, $columnBodyType, $columnImgPath)
              values("Lada Largus", "АвтоВаз", "B", 2015,
              "универсал", "${Strings.imgFolder + "/lada_largus.jpg"}")''');

        await db.rawInsert('''insert into $tableName(
              $columnModel, $columnManufacturer, $columnClass, 
              $columnYear, $columnBodyType, $columnImgPath)
              values("Lada Niva", "АвтоВаз", "B", 2015,
              "универсал", "${Strings.imgFolder + "/lada_niva.jpg"}")''');
      });
    });
  }

  Future checkOpen(String dbName) async {
    if (!isOpenedDb) {
      await open(dbName);
      isOpenedDb = true;
    }
  }

  Future deleteDatabase(String dbName) async {
    var dbPath = await getDatabasesPath();
    await deleteDatabase(join(dbPath, dbName));
  }

  Map<String, dynamic> carToMap(Car car) {
    var map = <String, dynamic>{
      columnModel: car.model,
      columnManufacturer: car.manufacturer,
      columnClass: car.carClass,
      columnYear: car.year,
      columnBodyType: car.bodyType,
      columnImgPath: car.imgPath
    };
    return map;
  }

  Car carFromMap(Map<String, dynamic> map) {
    return Car(map[columnModel], map[columnManufacturer], map[columnClass],
        map[columnYear], map[columnBodyType],
        imgPath: map[columnImgPath], id: map[columnId]);
  }

  Future<void> insert(Car car) async {
    await checkOpen(dbName);
    await db.insert(tableName, carToMap(car));
  }

  Future<void> delete(Car car) async {
    await checkOpen(dbName);
    await db.delete(tableName, where: '$columnId = ?', whereArgs: [car.id]);
  }

  Future<void> update(Car car) async {
    await checkOpen(dbName);
    await db.update(tableName, carToMap(car),
        where: '$columnId = ?', whereArgs: [car.id]);
  }

  Future<List<Car>> getCars() async {
    await checkOpen(dbName);

    List<Map> carsMap = await db.query(tableName, columns: null);

    List<Car> cars = [];
    for (var carMap in carsMap) {
      cars.add(carFromMap(carMap));
    }
    return cars;
  }

  Future close() async {
    if (isOpenedDb) {
      db.close();
    }
  }
}
