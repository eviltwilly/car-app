import 'dart:io';

import 'package:car_app/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Common {
  static final imgPicker = ImagePicker();

  static bool _isAssetPath(String imgPath) {
    return imgPath.startsWith(Strings.imgFolder);
  }

  static Widget getImage(String imgPath) {
    if (_isAssetPath(imgPath)) {
      return Image.asset(imgPath);
    } else if (imgPath.isEmpty) {
      return Image.asset(Strings.imgFolder + Strings.imgPlaceholder);
    } else {
      return Image.file(File(imgPath));
    }
  }

  static Future pickImageGallery(Function onPickedImage) async {
    final pickedFile = await imgPicker.getImage(source: ImageSource.gallery);
    onPickedImage(pickedFile.path);
  }

  static Future pickImageCamera(Function onPickedImage) async {
    final pickedFile = await imgPicker.getImage(source: ImageSource.camera);
    onPickedImage(pickedFile.path);
  }

  static void _popDialog(BuildContext context) => Navigator.of(context).pop();

  static Future<void> showDialogDelete(
      BuildContext context, int itemCount, Function onPressed) async {
    var itemCountTitle = "запись";

    if (itemCount >= 2 && itemCount <= 4) itemCountTitle = "записи";

    if (itemCount >= 5) itemCountTitle = "записей";

    AlertDialog dialog = new AlertDialog(
        title: Text(Strings.deleteTitle),
        content: Text(Strings.deleteContent + " $itemCount $itemCountTitle?"),
        actions: [
          FlatButton(
              child: Text(Strings.okAction),
              onPressed: () {
                onPressed(true);
                _popDialog(context);
              }),
          FlatButton(
              child: Text(Strings.cancelAction),
              onPressed: () => _popDialog(context)),
        ]);

    showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  static Future<void> showDialogOptions(
      BuildContext context, Function onPressed) async {
    AlertDialog dialog = new AlertDialog(
        title: Text(Strings.imageSelectTitle),
        content: Text(Strings.imageSelectContent),
        actions: [
          FlatButton(
              child: Text(Strings.cameraAction),
              onPressed: () {
                onPressed(Strings.cameraAction);
                _popDialog(context);
              }),
          FlatButton(
              child: Text(Strings.galleryAction),
              onPressed: () {
                onPressed(Strings.galleryAction);
                _popDialog(context);
              }),
          FlatButton(
              child: Text(Strings.cancelAction),
              onPressed: () => _popDialog(context)),
        ]);

    showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }
}
