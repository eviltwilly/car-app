import 'package:car_app/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'my_home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.appName,
      theme: ThemeData(
          primaryColor: Color.fromARGB(0xFF, 99, 0, 238),
          accentColor: Color.fromARGB(0xFF, 3, 218, 192),
          fontFamily: 'Roboto'),
      home: MyHomePage(title: Strings.appName),
    );
  }
}
