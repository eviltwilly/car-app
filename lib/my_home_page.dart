import 'package:car_app/car_database_helper.dart';
import 'package:car_app/strings.dart';
import 'package:flutter/material.dart';

import 'add_page.dart';
import 'car.dart';
import 'common.dart';
import 'edit_page.dart';
import 'help_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final carProvider = CarProvider();
  bool isLoading = true;

  List<Car> cars = [];
  List<bool> isSelectedItems = [];

  @override
  void initState() {
    super.initState();
    loadCars();
  }

  Future<void> loadCars() async {
    var cars = await carProvider.getCars();
    isSelectedItems = List<bool>.generate(cars.length, (index) => false);

    setState(() {
      this.cars = cars;
      this.isLoading = false;
    });
  }

  void _openAddPage() {
    var route = MaterialPageRoute(builder: (context) => AddPage(carProvider));
    Navigator.of(context).push(route).whenComplete(() => loadCars());
  }

  void _openEditPage(int i) {
    var route = MaterialPageRoute(
        builder: (context) => EditPage(this.cars[i], carProvider));
    Navigator.of(context).push(route).whenComplete(() => loadCars());
  }

  void _openHelpPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => HelpPage()));
  }

  ListView buildCarsList() {
    return ListView.builder(
      itemCount: cars.length,
      itemBuilder: (BuildContext context, int position) {
        return GestureDetector(
          onTap: () {
            if (isSelectedItems.any((element) => element)) {
              setState(() {
                isSelectedItems[position] = !isSelectedItems[position];
              });
            } else {
              _openEditPage(position);
            }
          },
          onLongPress: () {
            setState(() {
              isSelectedItems[position] = !isSelectedItems[position];
            });
          },
          child: getRow(position),
        );
      },
    );
  }

  Color getColor(int i) {
    return isSelectedItems[i] ? Color.fromARGB(0x55, 0, 0, 0) : null;
  }

  Widget getRow(int i) {
    return Card(
        elevation: 10.0,
        color: getColor(i),
        margin: EdgeInsets.only(bottom: 10, left: 6.0, right: 6.0, top: 10.0),
        child: Padding(
            padding: EdgeInsets.only(bottom: 16.0), child: buildCarColumn(i)));
  }

  Widget buildTextRow(String value) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, top: 10.0),
      child: Text(value),
    );
  }

  Widget buildCarColumn(int i) {
    var car = cars[i];

    return Column(
      children: [
        Container(
          foregroundDecoration: BoxDecoration(
            color: getColor(i),
          ),
          child: Common.getImage(cars[i].imgPath),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 16.0),
          child: Text(
            "Модель: ${car.model}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        buildTextRow("Производитель: ${car.manufacturer}"),
        buildTextRow("Класс: ${car.carClass}"),
        buildTextRow("Год: ${car.year}"),
        buildTextRow("Тип кузова: ${car.bodyType}"),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  Widget buildAction(IconData iconData, Function pressed) {
    return Container(
      margin: EdgeInsets.only(right: 3),
      child: IconButton(
        onPressed: () {
          pressed();
        },
        icon: Icon(iconData),
      ),
    );
  }

  void _deleteCars() {
    int itemCount = 0;
    isSelectedItems.forEach((element) {
      if (element) itemCount++;
    });
    Common.showDialogDelete(context, itemCount, (bool accept) {
      if (accept) {
        isSelectedItems.asMap().forEach((index, isSelected) {
          if (isSelected) {
            carProvider.delete(cars[index]);
          }
        });
        loadCars();
      }
    });
  }

  Future<bool> _onWillPop() async {
    if (isSelectedItems.any((element) => element)) {
      setState(() {
        isSelectedItems = List<bool>.generate(cars.length, (index) => false);
      });
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    var helpAction = buildAction(Icons.help, _openHelpPage);
    var deleteAction = buildAction(Icons.delete, _deleteCars);
    var actions = isSelectedItems.any((element) => element)
        ? [deleteAction, helpAction]
        : [helpAction];

    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(title: Text(widget.title), actions: actions),

          body: this.isLoading
              ? Center(child: CircularProgressIndicator())
              : buildCarsList(),
          floatingActionButton: FloatingActionButton(
            onPressed: _openAddPage,
            tooltip: Strings.addTooltip,
            child: Icon(
              Icons.add,
              color: Color.fromARGB(0xFF, 255, 255, 255),
            ),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ),
        onWillPop: _onWillPop);
  }
}
