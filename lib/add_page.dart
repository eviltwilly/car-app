import 'package:car_app/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'car.dart';
import 'car_data.dart';
import 'car_database_helper.dart';
import 'common.dart';

class AddPage extends StatefulWidget {
  final CarProvider carProvider;

  AddPage(this.carProvider);

  @override
  _AddPageState createState() => _AddPageState(carProvider);
}

class _AddPageState extends State<AddPage> {
  final CarProvider carProvider;

  _AddPageState(this.carProvider);

  String model = "";
  String manufacturer = "";
  String carClass = CarData.classes.first;
  int year = CarData.years.first;
  String bodyType = CarData.bodyTypes.first;
  String imgPath = "";

  bool isEmptyFields() {
    return model.isEmpty && manufacturer.isEmpty;
  }

  void _addCar() {
    carProvider.insert(
        Car(model, manufacturer, carClass, year, bodyType, imgPath: imgPath));
    Navigator.of(context).pop();
  }

  void _checkAction() {
    isEmptyFields() ? setState(() {}) : _addCar();
  }

  void _pickImage() {
    Common.showDialogOptions(context, (String itemSelected) {
      if (itemSelected == Strings.cameraAction) {
        Common.pickImageCamera((String path) => setState(() => imgPath = path));
      } else {
        Common.pickImageGallery(
            (String path) => setState(() => imgPath = path));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(Strings.addTitle), actions: [
        Container(
          margin: EdgeInsets.only(right: 3),
          child: IconButton(
            onPressed: _checkAction,
            icon: Icon(Icons.check),
          ),
        )
      ]),
      body: Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: SingleChildScrollView(
          child: buildCarData(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: Strings.pickImageTooltip,
        onPressed: _pickImage,
        child: Icon(
          Icons.add_a_photo,
          color: Color.fromARGB(0xFF, 255, 255, 255),
        ),
      ),
    );
  }

  Widget buildDropDownList(
      List<String> values, String selectedValue, Function changeValue) {
    return DropdownButton<String>(
      onChanged: (String newValue) {
        setState(() {
          changeValue(newValue);
        });
      },
      isExpanded: true,
      items: values.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Container(
            padding: EdgeInsets.all(3.0),
            child: Text(value, style: TextStyle(fontSize: 16.0)),
          ),
        );
      }).toList(),
      value: selectedValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 16.0,
    );
  }

  Widget buildTextField(Function onChanged, String hint, String text) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
      child: TextField(
        onChanged: onChanged,
        style: TextStyle(
          fontSize: 16.0,
        ),
        decoration: InputDecoration(
          hintText: hint,
          errorText: text.isEmpty ? Strings.errorEmptyField : null,
        ),
      ),
    );
  }

  Widget buildCarData() {
    return Column(
      children: [
        GestureDetector(
          child: Common.getImage(imgPath),
          onTap: _pickImage,
        ),
        buildTextField((String newValue) {
          if (newValue.isNotEmpty) {
            setState(() {
              model = newValue;
            });
          }
        }, Strings.hintModel, model),
        buildTextField((String newValue) {
          if (newValue.isNotEmpty) {
            setState(() {
              manufacturer = newValue;
            });
          }
        }, Strings.hintManufacturer, manufacturer),
        Container(
            margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
            child:
                buildDropDownList(CarData.classes, carClass, (String newValue) {
              carClass = newValue;
            })),
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
          child: buildDropDownList(
              CarData.years.map((e) => e.toString()).toList(), year.toString(),
              (String newValue) {
            year = int.parse(newValue);
          }),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
          child:
              buildDropDownList(CarData.bodyTypes, bodyType, (String newValue) {
            bodyType = newValue;
          }),
        ),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}
